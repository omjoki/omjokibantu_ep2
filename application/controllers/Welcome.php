<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Omjoki_model', 'ojq');
	}
	public function index()
	{
		// $this->load->view('welcome_message');
		$data['user'] = $this->db->get('tb_user')->result_array();
		// var_dump($data['user']);
		$this->load->view('bulan', $data);
	}

	public function bulan($bulan, $tahun)
	{
		$data['user'] = $this->db->get('tb_user')->result_array();
		$data['databulan'] = $this->ojq->getJumlahhari($bulan);
		$data['tahun'] = $tahun;
		$data['bulan'] = $bulan;
		$this->load->view('index', $data);
	}
}
