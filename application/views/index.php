<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

</head>

<body>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Jabatan</th>
                <?php for ($jumlahhari = 1; $jumlahhari < $databulan + 1; $jumlahhari++) : ?>
                    <th scope="col"><?= $jumlahhari; ?></th>
                <?php endfor; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($user as $u) : ?>
                <tr>
                    <th scope="row"><?= $u['id_user']; ?></th>
                    <td><?= $u['nama_user']; ?></td>
                    <td><?= $u['jabatan']; ?></td>
                    <?php for ($jumlahhari2 = 1; $jumlahhari2 < $databulan + 1; $jumlahhari2++) : ?>
                        <?php
                        $tanggal = $tahun . '-' . $bulan . '-' . $jumlahhari2;
                        $dataabsen = $this->db->where('tanggal', $tanggal);
                        $dataabsen = $this->db->where('id_user', $u['id_user']);
                        $dataabsen = $this->db->get('tb_absen')->row_array();
                        ?>
                        <td>
                            <?php if (!$dataabsen) : ?>
                                -
                            <?php else : ?>
                                keterangan : <?= $dataabsen['keterangan']; ?>
                                <br>
                                Jam Masuk : <?= $dataabsen['jam_masuk']; ?>
                                <br>
                                Jam Pulang : <?= $dataabsen['jam_pulang']; ?>
                            <?php endif; ?>
                        </td>
                    <?php endfor; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>

</html>